# California license plates game

**Warning: this dataset contains vulgar and offensive language (quite a lot of it).**

This is a web-based game where you're shown a proposed vanity plate, and have to guess whether or not the California DMV accepted or rejected it.

You can play the game here: https://julianh.dev/other/license-plate-game/


Originally forked from this database: https://github.com/veltman/ca-license-plates